extern crate glutin;
#[macro_use]
extern crate glium;
extern crate nalgebra;
extern crate num;

pub type Point = nalgebra::Pnt3<f64>;
pub type Vector = nalgebra::Vec3<f64>;
pub struct Ray {
    pub origin: Point,
    pub dir: Vector
}
pub struct Plane {
    pub normal: Vector,
    pub d: f64
}

use num::Float;
use nalgebra::{cross, dot};
use nalgebra::{ApproxEq};

struct EdgeIterator<T: Iterator> where T::Item: Clone
{
    first: Option<T::Item>,
    iter: std::iter::Peekable<T>
}

impl<T:Iterator> Iterator for EdgeIterator<T> where T::Item: Clone
{
    type Item = (T::Item, T::Item);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|v|(v,
                                 self.iter.peek()
                                 .cloned()
                                 .or_else(|| self.first.take())
                                 .unwrap()))
    }
}

trait EdgeIterExt<T: Iterator> where T::Item: Clone {
    fn edges(self) -> EdgeIterator<T>;
}

impl<T: Iterator> EdgeIterExt<T> for T where T::Item: Clone {
    fn edges(self) -> EdgeIterator<T> {
        let mut iter = self.peekable();
        EdgeIterator {
            first: iter.peek().cloned(),
            iter: iter
        }
    }
}

fn normalized<T: nalgebra::Norm<N>, N: nalgebra::BaseFloat>(mut v: T) -> T {
    v.normalize_mut();
    v
}

fn ray_plane(ray: &Ray, plane: &Plane) -> Option<f64> {
    let pdist = dot(ray.origin.as_vec(), &plane.normal);
    let pdir = dot(&ray.dir, &plane.normal);
    if pdist.approx_eq(&plane.d) {
        Some(0.0)
    } else if pdir.approx_eq(&0.0) {
        None
    } else {
        Some((plane.d - pdist) / pdir)
    }
}

// This assumes a certain winding order
pub fn ray_convex_polygon(ray: &Ray, poly: &[Point]) -> bool {
    assert!(poly.len() >= 3);
    //For pathological polygons this may have problems
    let normal = cross(&(poly[1] - poly[0]), &(poly[2] - poly[1]));
    let plane = Plane {
        normal: normal,
        d: dot(poly[0].as_vec(), &normal)
    };
    let plane_point = match ray_plane(ray, &plane) {
        Some(t) if t > 0.0 => ray.origin + ray.dir * t,
        _ => return false
    };
    for (&p1, &p2) in poly.iter().edges() {
        let e = p2 - p1;
        let e_r = cross(&e, &normal);
        if dot(&(plane_point - p1), &e_r) < 0.0 { //Outside the edge
            return false
        }
    }
    true
}


//This function deals in planes, but it should be easy to preprocess a polyhedron to produce planes
pub fn ray_convex_polyhedron(ray: &Ray, poly: &[Plane]) -> bool {
    assert!(poly.len() >= 4);
    let mut t_min = f64::neg_infinity();
    let mut t_max = f64::infinity();
    for plane in poly {
        ray_plane(ray, plane).map(|t| {
            if dot(&plane.normal, &ray.dir) > 0.0 {
                t_min = t_min.max(t)
            } else {
                t_max = t_max.min(t)
            }
        });
        if t_min > t_max {
            return false
        }
    }
    t_min >= 0.0
}

fn main() {
    use nalgebra::{Mat4, Vec3, Iso3, ToHomogeneous, Inv, BaseFloat, Rotation, PerspMat3};
    use num::Zero;
    use glium::{DisplayBuild, Surface, DrawParameters, BackfaceCullingMode, PolygonMode, DepthTest};

    let display = glutin::WindowBuilder::new()
        .with_title("".to_string())
        .with_dimensions(800, 600)
        .with_depth_buffer(24)
        .with_srgb(Some(true))
        .build_glium().unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 3],
        normal: [f32; 3]
    }

    implement_vertex!(Vertex, position, normal);
    fn to_vec3((x, y, z): (f32, f32, f32)) -> Vec3<f32> {
        Vec3::new(x, y, z)
    }
    let shape: Vec<_> = [((0.0, 0.0, 0.0),
                         (0.0, 1.0, 0.0),
                         (1.0, 0.0, 0.0)),

                         ((0.0, 0.0, 0.0),
                         (0.0, 0.0, 1.0),
                         (0.0, 1.0, 0.0)),

                         ((0.0, 0.0, 0.0),
                         (1.0, 0.0, 0.0),
                         (0.0, 0.0, 1.0)),

                         ((1.0, 0.0, 0.0),
                         (0.0, 1.0, 0.0),
                         (0.0, 0.0, 1.0))]
        .iter()
        .flat_map(|&(a, b, c)| {
            let (va, vb, vc) = (to_vec3(a), to_vec3(b), to_vec3(c));
            let n = normalized(cross(&(vb - va), &(vc - va)));
            vec![(va, n), (vb, n), (vc, n)].into_iter()
        }).map(|(v, n)| Vertex {
            position: [v.x, v.y, v.z],
            normal: [n.x, n.y, n.z]
        })
        .collect();

    let vertex_buffer = glium::VertexBuffer::new(&display, shape);
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);

    let mut world_mat = Iso3::new(Vec3::new(0.0, 0.0, 0.0), Vec3::zero());
    let view = Iso3::new(Vec3::new(0.0, 0.6, -3.0), Vec3::zero());
    let proj = PerspMat3::new(1.0, f32::pi() / 3.0, 0.01, 100.0);

    let vertex_shader_src = r#"
        #version 140

        uniform mat4 inv_world;
        uniform mat4 wvp;

        in vec3 position;
        in vec3 normal;

        out vec3 fnormal;

        void main() {
            fnormal = normalize((vec4(normal, 0.0) * inv_world).xyz);
            gl_Position = wvp * vec4(position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 140

        uniform vec3 tri_colour;

        in vec3 fnormal;

        out vec4 colour;

        void main() {
            float diffuse = max(dot(normalize(fnormal), normalize(vec3(1.0, 4.0, -1.0))), 0.0);
            colour = vec4(pow((diffuse + 0.05) * tri_colour, vec3(1.0/2.2)), 1.0);
        }
    "#;

    let program = glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap();

    loop {
        world_mat.prepend_rotation_mut(&Vec3::new(0.0, f32::pi() / 600.0, 0.0));
        let world = world_mat.to_homogeneous();
        let wvp: Mat4<f32> = proj.to_mat() * view.inv().unwrap().to_homogeneous() * world;
        let uniforms = uniform! {
            inv_world: world.inv().unwrap(),
            wvp: wvp,
            tri_colour: [0.0, 1.0, 0.4]
        };

        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.clear_depth(1.0);
        target.draw(&vertex_buffer, &indices, &program, &uniforms,
                    &DrawParameters {
                        depth_test: DepthTest::IfLess,
                        depth_write: true,
                        backface_culling: BackfaceCullingMode::CullClockWise,
                        //polygon_mode: PolygonMode::Line,
                        ..std::default::Default::default()
                    }).unwrap();
        target.finish();

        for event in display.poll_events() {
            match event {
                glutin::Event::Closed => return,
                _ => ()
            }
        }
    }
}
