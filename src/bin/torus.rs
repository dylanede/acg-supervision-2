extern crate glutin;
#[macro_use]
extern crate glium;
extern crate nalgebra;
extern crate num;

use nalgebra::{cross};

fn normalized<T: nalgebra::Norm<N>, N: nalgebra::BaseFloat>(mut v: T) -> T {
    v.normalize_mut();
    v
}

fn main() {
    use nalgebra::{Mat4, Vec3, Iso3, ToHomogeneous, Inv, BaseFloat, Rotation, Translation, PerspMat3};
    use glium::{DisplayBuild, Surface, DrawParameters, BackfaceCullingMode, PolygonMode, DepthTest};
    use num::Zero;
    let display = glutin::WindowBuilder::new()
        .with_title("".to_string())
        .with_dimensions(800, 800)
        .with_depth_buffer(24)
        .with_srgb(Some(true))
        .build_glium().unwrap();

    let program = glium::Program::new(&display, glium::program::SourceCode {
        vertex_shader: r#"
#version 140
void main() {}
"#,
        tessellation_control_shader: Some(r#"
#version 400

layout(vertices = 4) out;

uniform int tess_level_x = 64;
uniform int tess_level_y = 16;

void main() {
    gl_TessLevelOuter[0] = tess_level_y;
    gl_TessLevelOuter[1] = tess_level_x;
    gl_TessLevelOuter[2] = tess_level_y;
    gl_TessLevelOuter[3] = tess_level_x;
    gl_TessLevelInner[0] = tess_level_x;
    gl_TessLevelInner[1] = tess_level_y;
}
"#),
        tessellation_evaluation_shader: Some(r#"
#version 400

layout(quads, equal_spacing) in;

uniform mat4 wvp;
uniform mat4 world;
uniform vec3 camera_pos;

#define pi 3.141592653589793238462643383279

out vec2 uv;
out vec3 Eye;

vec3 f(vec2 uv) {
    vec2 phase = uv * 2.0 * pi;
    float from_centre = 0.75 + 0.25 * cos(phase.y);
    return vec3(from_centre * cos(phase.x), from_centre * sin(phase.x), 0.25 * sin(phase.y));
}

void main() {
    vec3 position = f(gl_TessCoord.xy);
    uv = gl_TessCoord.xy;
    Eye = camera_pos - (world * vec4(position, 1.0)).xyz;
    gl_Position = wvp * vec4(position, 1.0);
}
"#),
        geometry_shader: None,
        fragment_shader: r#"
#version 140

#define pi 3.141592653589793238462643383279

uniform vec3 in_colour;
uniform mat4 inv_world;

in vec2 uv; in vec3 Eye;

out vec4 colour;

vec3 local_n(vec2 uv) {
    vec2 phase = uv * 2.0 * pi;
    float from_centre = cos(phase.y);
    return vec3(from_centre * cos(phase.x), from_centre * sin(phase.x), sin(phase.y));
}

void main() {
    vec3 L = normalize(vec3(0.1, 1.0, 0.0));
    vec3 N = normalize((vec4(local_n(uv), 0.0) * inv_world).xyz);
    vec3 V = normalize(Eye);
    vec3 H = normalize(V + L);
    float spec = pow(max(dot(N, H), 0.0), 15.0);
    float diffuse = max(dot(N, L), 0.0);
    colour = vec4(pow(0.5 * in_colour * diffuse + 0.5 * vec3(spec), vec3(2.2)), 1.0);
}
"#
    }).unwrap();

    let mut world_mat = Iso3::new(Vec3::new(0.0, 0.0, 0.0), Vec3::zero());
    let view = Iso3::new(Vec3::new(0.0, 0.0, -3.0), Vec3::zero());
    let proj = PerspMat3::new(1.0, f32::pi() / 3.0, 0.01, 100.0);

    loop {
        world_mat.prepend_rotation_mut(&Vec3::new(0.0, f32::pi() / 600.0, 0.0));
        let world = world_mat.to_homogeneous();
        let wvp: Mat4<f32> = proj.to_mat() * view.inv().unwrap().to_homogeneous() * world;
        let uniforms = uniform! {
            inv_world: world.inv().unwrap(),
            world: world,
            camera_pos: view.translation(),
            wvp: wvp,
            in_colour: [0.0, 1.0, 0.4]
        };

        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.clear_depth(1.0);
        target.draw(glium::vertex::EmptyVertexAttributes{ len: 4 },
                    &glium::index::NoIndices(glium::index::PrimitiveType::Patches{ vertices_per_patch: 4 }),
                    &program, &uniforms,
                    &DrawParameters {
                        depth_test: DepthTest::IfLess,
                        depth_write: true,
                        backface_culling: BackfaceCullingMode::CullClockWise,
                        //polygon_mode: PolygonMode::Line,
                        ..std::default::Default::default()
                    }).unwrap();
        target.finish();

        for event in display.poll_events() {
            match event {
                glutin::Event::Closed => return,
                _ => ()
            }
        }
    }
}
